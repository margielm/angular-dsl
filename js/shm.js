angular.module("shmDirectives", [])
    .directive("redPanel", function () {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                $(element)
                    .css("background-color", attrs.redPanel)
                    .css("color", attrs.textColor);
            }
        }
    })
    .directive("shmRandom", function () {
        return {
            scope: false,
            template: "<div>{{number}}</div>",
            link: function (scope) {
                scope.number = Math.random();
            }
        };
    })
    .directive("shmEditableLabel", function () {
        return {
            restrict: "E",
            scope: {content: "@"},
            template: "<div ng-dblclick='edit = !edit' >" +
                "<div  ng-hide='edit'>" +
                "{{content}}" +
                "</div>" +
                "<input type='text' ng-model='content' ng-show='edit' >  " +
                "</div>",
            link: function (scope) {
                scope.edit = false;
            }
        };
    })
    .directive("shmHelloTag", function () {
        return {
            scope: {
            },
            template: "<div class='hello-tag'>" +
                "Helloa<br />My Name is " +
                "<div class='inner'>" +
                "<span ng-transclude></span>   " +
                "</div>" +
                "</div>",
            restrict: "EA",
            transclude: true
        };
    });